'use strict';

export default class MainClass {
  constructor() {
    this.appName = 'Node Simple Template';
  }

  async start() {
    console.log(`Hello from '${this.appName}'`);
  }
}

function bootstrap() {
  const mainClass = new MainClass();
  mainClass.start();
}

bootstrap();