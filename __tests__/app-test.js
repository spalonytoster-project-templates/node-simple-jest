/* globals describe, context, it */
'use strict';
import {expect, jest, test} from '@jest/globals';
import MainClass from '../src/app.js';

const app = new MainClass();

test('Should bootstrap', () => {
  let spy = jest.spyOn(app, 'start');
  
  app.start();

  expect(spy).toHaveBeenCalled();
});